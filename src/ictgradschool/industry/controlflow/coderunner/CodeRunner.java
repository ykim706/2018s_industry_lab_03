package ictgradschool.industry.controlflow.coderunner;

/**
 * Please run TestCodeRunner to check your answers
 */
public class CodeRunner {
    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = "";
        if (firstName.equals(secondName)) {
            message = "Same name";
        } else if (firstName.substring(0, 1).equals(secondName.substring(0, 1))) {
            message = "Same first letter";
        } else {
            message = "No match";
        }


        // TODO write answer to Q1
        return message;
    }
    /** areSameName(String, String) => String **/


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {
        boolean leapYear = false;
        if (year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)) {
            return true;
        }
        // TODO write answer for Q2
        return leapYear;
    }
    /** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {
        int reverseNum = 0;
        while (number != 0) {
            int one = number % 10;
            number = number / 10;
            reverseNum = reverseNum * 10;
            reverseNum = reverseNum + one;
        }
        // TODO write answer for Q3
        return reverseNum;
    }
    /** reverseInt(int) => void **/


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        String reverseStr = "";
        for(int i = str.length() - 1 ; i >= 0; i--) {
            String lastLetter = String.valueOf(str.charAt(i));
            reverseStr += lastLetter;
        }
        // TODO write answer for Q4
        return reverseStr;
    }
    /** reverseString(String) => void **/


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        String multiplicationTable = "";
        for (int i = 1; i <= num; i++) {
            for (int n = 1; n <= num; n++) {
//                System.out.println(i * n + "\n");
                multiplicationTable = multiplicationTable + i * n + " ";
            }
            multiplicationTable = multiplicationTable.trim();

            if(i != num){
                multiplicationTable += "\n";
            }
        }


        // TODO write answer for Q5
        return multiplicationTable;
    }
    /** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        String columnName = "";
        while (num > 0) {
            char character = (char) ('A' + (num - 1) % 26);
            num = (num - 1) / 26;
            columnName = character + columnName;
        }

        if (columnName.isEmpty()) {
            columnName = "Input is invalid";
        }
        // TODO write answer for Q6
        return columnName;
    }
    /** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        if(num == 2){
            return true;
        }else if(num < 2){
            return false;
        }
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
        // TODO write answer for Q7
    }

    /** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {

        int palindromeNum = 0;
        int tempNum = num;
        while (num != 0) {
            int one = num % 10;
            num = num / 10;
            palindromeNum = palindromeNum * 10 + one;
        }
        // TODO write answer for Q8
        return tempNum == palindromeNum;
    }
    /** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {
        String palindromeStr = "";
        str = str.replaceAll("[, ]", "");
        String newStr = str;
        while (!str.isEmpty()) {
            palindromeStr += str.substring(str.length() - 1);
            str = str.substring(0, str.length() - 1);
        }
        // TODO write answer for Q9
        return newStr.equals(palindromeStr);
    }
    /** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        String primesStr = "";
        for (int i = 2; i <= num; i++) {
            if (isPrime(i)) {
                primesStr += i + " ";
            }
        }
        if(primesStr.isEmpty()){
            return "No prime number found";
        }
        // TODO write answer for Q10
        return primesStr.trim();
    }
}
